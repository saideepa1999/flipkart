package com.bk.leave.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bk.leave.entity.LeaveDetails;
import com.bk.leave.service.CommonService;

@RestController
//@RequestMapping(value="/CommonController")
public class CommonController {
	@Autowired
	CommonService ser;
	
	@PostMapping(value="/applyLeave")
	public void applyLeave(@RequestBody LeaveDetails ld) {
		ser.applyLeave(ld);
	}

}
